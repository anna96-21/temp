import { Component } from '@angular/core';
import { List } from './list';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title : string = "";
  public des: string = "";
  public 
  list: List = {
    title: [],
    count: [],
    checked: []
  }

  addList(){
    this.list.title.push(this.title);
    console.log(this.list.title);
    this.title = "";
    this.list.checked.push(false);
  }
  addCheck(i){
    this.list.checked[i] = !this.list.checked[i];
    console.log(this.list.checked);
    console.log(this.list.checked);
  }

  deleteAll(){
    for(let i = 0; i < this.list.checked.length; i++){
      if(this.list.checked[i] === true){
        this.list.title.splice(i,1);
        this.list.checked.splice(i,1);
       }
       
    }
    
  }

  myEdit(i){
    this.title = this.list.title[i];
    console.log(this.list.title[i]);
  }

  done(i){
    console.log(i);
    this.list.title[i] = this.title;
    this.list.title.splice(i,1);
    //this.list.title.push(this.title);
    
    console.log(this.list.title);
    console.log(this.title);
    this.title="";
  }
}
