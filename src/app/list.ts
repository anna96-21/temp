export class List{
    title: string[];
    count: number[];
    checked: boolean[];
    constructor(
        title: string[],
        count: number[],
        checked: boolean[]){
            this.title = title;
            this.count = count;
            this.checked = checked;
    }


}